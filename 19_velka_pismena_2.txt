Vlak má zase zpoždění. Jedeme na výlet a máme v Jindřichově Hradci jen pět minut na přestup.
Jsme teprve na nádraží v Ostravě. Na tabuli už naskočilo zpoždění deset minut.
Ach jo. Vlak konečně přijíždí.
Cesta plyne a my s napětím a nervozitou sledujeme cedule zavěšené na nádražních štítech, kolem kterých projíždíme: Studénka, Suchdol nad Odrou, Hranice na Moravě, Přerov, Olomouc, Zábřeh na Moravě.
Ještě než dojíždíme do Pardubic, všimnu si, že jsem se předtím spletl.
Nepřestupujeme v Jindřichově Hradci, ale v Pardubicích.
Odtud pokračujeme do Hradce Králové.
Navazující vlak jsme stihli.
Byl to „osobák“, a tak čekal.
Pokračovali jsme směle dál: Pardubice – Rosice nad Labem, Čeperka a konečně Hradec Králové.
Dojeli jsme! Juchů! 