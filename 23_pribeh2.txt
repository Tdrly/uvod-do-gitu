Blížící se podzim jako by chtěl vytvořit barevnou tečku za letními prázdninami. 
Vítr honil listy brázdami zoraných polí, zemědělci seli ozimy. 
Seděli jsme u ohýnku a opékali poslední špekáčky letošního roku. 
Prostupovalo námi teplo a šťáva ze špekáčků čas od času zaprskala, jak ji lačně olizovaly plameny. 
Vzpomínali jsme na všechny milé lapálie, které se nám během léta při společných akcích přihodily. 
Jen Jirka seděl zamyšleně a nepřidal se ke kolektivnímu veselí prošpikovanému salvami smíchu. 
Žhavil mozkové závity… 
